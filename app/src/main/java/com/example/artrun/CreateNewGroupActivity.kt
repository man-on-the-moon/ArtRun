package com.example.artrun

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.widget.Toast
import com.example.artrun.databinding.ActivityCreateNewGroupBinding
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import kotlin.concurrent.thread


class CreateNewGroupActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCreateNewGroupBinding
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateNewGroupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        
        binding.btnCancelCreateNewGroup.setOnClickListener {
            finish()
        }
        binding.btnOKCreateNewGroup.setOnClickListener {
            if (isNetworkConnected(this)){
                mysqlCreateNewGroup(binding.editTextGroupNameCreateNewGroup.text.toString())
                Toast.makeText(this, "成功创建", Toast.LENGTH_SHORT).show()
                finish()
            }
            else {
                Toast.makeText(this, "网络状态异常", Toast.LENGTH_SHORT).show()
            }
        }
    }
    
    fun isNetworkConnected(context: Context?): Boolean {
        if (context != null) {
            val mConnectivityManager = context
                .getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val mNetworkInfo = mConnectivityManager.activeNetworkInfo
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable
            }
        }
        return false
    }
    
    private fun mysqlCreateNewGroup(groupName: String) {
        thread {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver")
            // 建立连接
            val cn = DriverManager.getConnection(
                "jdbc:mysql://112.74.87.145:3306/runart",
                "runartadmin", "runartpassword"
            )
    
            val psCreateUser: PreparedStatement = cn.prepareStatement(
                "INSERT INTO `Group` (`grpName`) VALUES (?)"
            )
            psCreateUser.setString(1, groupName)
            psCreateUser.executeUpdate()
            psCreateUser.close()
    
            // 新建专属组表
            val cs = cn.createStatement()
            val resultSet: ResultSet = cs.executeQuery(
                "SELECT * FROM `Group`"
            )
            if (resultSet.last()) {
                val grpId = resultSet.getInt("id")
                val psCreateUserGrp: PreparedStatement = cn.prepareStatement(
                    "CREATE TABLE `Grp_$grpId` (" +
                            "  `id` int NOT NULL primary key AUTO_INCREMENT COMMENT 'Primary Key'," +
                            "  `userId` int NOT NULL," +
                            "  `lati` varchar(255) NOT NULL," +
                            "  `longi` varchar(255) NOT NULL" +
                            ") default charset utf8;"
                )
                psCreateUserGrp.executeUpdate()
                if (!psCreateUserGrp.isClosed){
                    psCreateUserGrp.close()
                }
            }
            if (!cn.isClosed) {
                cn.close()
            }
        }
    }
}