package com.example.artrun

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.widget.Toast
import com.example.artrun.databinding.ActivityLoginBinding
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import kotlin.concurrent.thread

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    
    val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                777 -> {
                    // TODO: 登录成功
                    val userId = msg.data.getInt("userid") ?: -1
                    localAccount(binding.editTextAccountLogin.text.toString(), userId)
                    Toast.makeText(this@LoginActivity, "登录成功", Toast.LENGTH_SHORT)
                        .show()
                }
                233 -> {
                    // TODO: 无账号
                    val userId = msg.data.getInt("userid") ?: -1
                    localAccount(binding.editTextAccountLogin.text.toString(), userId)
                    Toast.makeText(this@LoginActivity, "注册并登录成功", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        
        binding.btnEnterLogin.setOnClickListener {
            if (isNetworkConnected(this)){
                mysqlLogin(binding.editTextAccountLogin.text.toString())
            }
            else {
                Toast.makeText(this, "网络状态异常", Toast.LENGTH_SHORT).show()
            }
        }
        
        binding.btnExitLoginActivity.setOnClickListener {
            finish()
        }
    }
    
    fun isNetworkConnected(context: Context?): Boolean {
        if (context != null) {
            val mConnectivityManager = context
                .getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val mNetworkInfo = mConnectivityManager.activeNetworkInfo
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable
            }
        }
        return false
    }
    
    private fun mysqlLogin(account: String) {
        thread {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver")
            // 建立连接
            val cn = DriverManager.getConnection(
                "jdbc:mysql://112.74.87.145:3306/runart",
                "runartadmin", "runartpassword"
            )
            
            val stateTag: Int
            val tmpBundle = Bundle()
            val cs = cn.createStatement()
            val resultSet: ResultSet = cs.executeQuery(
                "SELECT * FROM `User` WHERE `account` = '$account'"
            )
            if (resultSet.first()) {
                stateTag = 777
                tmpBundle.putInt("userid", resultSet.getInt("id"))
            } else {
                stateTag = 233
                // User新增用户
                val psCreateUser: PreparedStatement = cn.prepareStatement(
                    "INSERT INTO `User` (`account`) VALUES (?)"
                )
                psCreateUser.setString(1, account)
                psCreateUser.executeUpdate()
                psCreateUser.close()
                
                val newUserResultSet: ResultSet = cs.executeQuery(
                    "SELECT * FROM `User` WHERE `account` = '$account'"
                )
                if (newUserResultSet.first()) {
                    tmpBundle.putInt("userid", newUserResultSet.getInt("id"))
                }
                if (!newUserResultSet.isClosed) {
                    newUserResultSet.close()
                    psCreateUser.close()
                }
            }
            resultSet.close()
            cs.close()
            val msg = Message()
            msg.what = stateTag
            msg.data = tmpBundle
            handler.sendMessage(msg) // 将Message对象发送出去
            
            if (!cn.isClosed) {
                cn.close()
            }
        }
    }
    
    private fun localAccount(userAccount: String, userId: Int) {
        // 数据库 读取 已输入账号是否存在数据库
        // init 数据库
        val dbHelper = AppDatabaseHelper(this, "BookStore.db", 1)
        val db = dbHelper.writableDatabase
        // 读取数据
        val cursor = db.rawQuery("select * from User where account = ?", arrayOf(userAccount))
        if (cursor.moveToFirst()) {
            // 更新数据表中该账号登陆状态(1,0)->(sign in, sign out)
            db.execSQL("update User set login_tag = ? where account = ?", arrayOf(1, userAccount))
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            // 本地账号不存在 -> 根据edittext中数据创建 new user ,并设置登陆状态为1
            if (userId > 0) {
                db.execSQL(
                    "insert into User (id, account, login_tag) values(?, ?, 1)",
                    arrayOf(userId, userAccount)
                )
            } else {
                db.execSQL(
                    "insert into User (account, login_tag) values(?, 1)",
                    arrayOf(userAccount)
                )
            }
            
            setResult(Activity.RESULT_OK)
            finish()
        }
        cursor.close()
    }
}