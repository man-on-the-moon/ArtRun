package com.example.artrun

import com.baidu.mapapi.model.LatLng
import java.util.ArrayList

class Data(val userId:Int, val points: ArrayList<LatLng>)