package com.example.artrun

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.baidu.location.BDAbstractLocationListener
import com.baidu.location.BDLocation
import com.baidu.location.LocationClient
import com.baidu.location.LocationClientOption
import com.baidu.mapapi.map.*
import com.baidu.mapapi.model.LatLng
import com.example.artrun.databinding.ActivityMapAcitivityBinding
import android.widget.Toast

import java.util.ArrayList
import com.baidu.mapapi.map.PolylineOptions

import com.baidu.mapapi.map.OverlayOptions
import android.net.ConnectivityManager
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log

import com.baidu.mapapi.map.MyLocationConfiguration
import com.baidu.mapapi.map.Stroke

import com.baidu.mapapi.map.PolygonOptions
import com.baidu.mapapi.map.MarkerOptions

import com.baidu.mapapi.map.Polyline

import com.baidu.mapapi.map.BitmapDescriptorFactory

import com.baidu.mapapi.map.MapStatusUpdateFactory
import com.example.artrun.MyApplication.Companion.context

import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import kotlin.concurrent.thread


private lateinit var binding: ActivityMapAcitivityBinding

@SuppressLint("StaticFieldLeak")
private lateinit var mMapView: MapView
private lateinit var mBaiduMap: BaiduMap

@SuppressLint("StaticFieldLeak")
private lateinit var mLocationClient: LocationClient
private lateinit var option: LocationClientOption
private lateinit var myLocationListener: MyLocationListener
private var isFirstLocate: Boolean = true
private var isFirstLoc: Boolean = true
private lateinit var lastLocation: LatLng
private var isOnDraw: Boolean = false
private lateinit var last: LatLng
private var resourceId: String = "localhost"
private var resourceAccount: String = "localhost"
private var grpId: Int = 0
private var idTag: Int = 0


class MapActivity : AppCompatActivity() {
    companion object {
        var Datalist: ArrayList<Data> = ArrayList()
        var zoom = 19f
    }
    
    private lateinit var binding: ActivityMapAcitivityBinding
    
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapAcitivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val dbHelper = AppDatabaseHelper(this, "BookStore.db", 1)
        val db = dbHelper.writableDatabase
        getLoginTag(db)
        idTag = 0
        
        binding.txtMapMapActivity.text = "$resourceAccount ($resourceId)"
        
        mMapView = binding.bmapViewMap
        Datalist.clear()
        var points: ArrayList<LatLng> = ArrayList()
        if (resourceId != "localhost"){
            Datalist.add(Data(resourceId.toInt(), points))
        } else {
            Datalist.add(Data(0, points))
        }
        //开启定位
        mBaiduMap = mMapView.map
        mBaiduMap.mapType = BaiduMap.MAP_TYPE_NORMAL
        mBaiduMap.isMyLocationEnabled = true
        val settings = mBaiduMap.uiSettings
        settings.setAllGesturesEnabled(true)
        mBaiduMap.setMyLocationConfiguration(
            MyLocationConfiguration(
                MyLocationConfiguration.LocationMode.FOLLOWING, true, null
            )
        )
        
        //定位初始化
        mLocationClient = LocationClient(this)
        //通过LocationClientOption设置LocationClient相关参数
        option = LocationClientOption()
        //初始化
        initOption(option)
        mLocationClient.setLocOption(option)
        //注册LocationListener监听器
        myLocationListener = MyLocationListener()
        mLocationClient.registerLocationListener(myLocationListener)
        //开启地图定位图层
        mLocationClient.start()
        
        //回到当前位置
        binding.btnBackToNow.setOnClickListener {
            isFirstLocate = true
            //startVoiceDialog()
        }

        //绘制结果
        binding.btnOK.setOnClickListener {
            val intentActivityDrawResultBinding = Intent(this,DrawResultActivity::class.java)
            startActivity(intentActivityDrawResultBinding)
        }
        //绘制图像监听
        binding.locationMark.setOnClickListener {
            isOnDraw = !isOnDraw
            if (isOnDraw) {
                Toast.makeText(this, "开始绘制", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "停止绘制", Toast.LENGTH_SHORT).show()
            }
        }
        
        //退出
        binding.btnExitMapActivity.setOnClickListener {
            Datalist.clear()
            isFirstLoc = true
            finish()
        }
    }
    
    override fun onResume() {
        super.onResume()
        Log.d("test","paole")
        isFirstLocate = true
        mMapView.onResume()
    }
    
    override fun onPause() {
        super.onPause()
        mMapView.onPause()
    }
    
    //释放资源
    override fun onDestroy() {
        super.onDestroy()
        mLocationClient.stop()
        mBaiduMap.isMyLocationEnabled = false
        mMapView.removeAllViews()
        mMapView.onDestroy()
        Datalist.clear()
        isFirstLoc = true
    }

    @SuppressLint("Range")
    private fun getLoginTag(db: SQLiteDatabase) {
        val cursorLoginTag = db.rawQuery("select * from User where login_tag = 1", null)
        resourceId = if (cursorLoginTag.moveToFirst()) {
            cursorLoginTag.getString(cursorLoginTag.getColumnIndex("id"))
        } else {
            "localhost"
        }
        resourceAccount = if (cursorLoginTag.moveToFirst()) {
            cursorLoginTag.getString(cursorLoginTag.getColumnIndex("account"))
        } else {
            "Local User"
        }
        grpId = if (cursorLoginTag.moveToFirst()) {
            cursorLoginTag.getInt(cursorLoginTag.getColumnIndex("grpId"))
        } else {
            0
        }
        cursorLoginTag.close()
    }

}

class MyLocationListener : BDAbstractLocationListener() {
    val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                666 -> {
                    
                    val resourceId = msg.data.getInt("userId")
                    val resourceLati = msg.data.getString("lati") ?: ""
                    val resourceLongi = msg.data.getString("longi") ?: ""
                    // 对数据进行本地化处理
                    // id判断
                    var isFlag = true
                    for (item in MapActivity.Datalist){
                        if (item.userId == resourceId){
                            // if id exist ,add arrylist
                            item.points.add(LatLng(resourceLati.toDouble(), resourceLongi.toDouble()))
                            isFlag = false
                            break
                        }
                    }
                    // else
                    if (isFlag){
                        MapActivity.Datalist.add(Data(resourceId, points = ArrayList<LatLng>()))
                        MapActivity.Datalist[MapActivity.Datalist.size-1].points.add(LatLng(resourceLati.toDouble(), resourceLongi.toDouble()))
                    }
                }
            }
        }
    }

    fun isNetworkConnected(context: Context?): Boolean {
        if (context != null) {
            val mConnectivityManager = context
                .getSystemService(AppCompatActivity.CONNECTIVITY_SERVICE) as ConnectivityManager
            val mNetworkInfo = mConnectivityManager.activeNetworkInfo
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable
            }
        }
        return false
    }

    private fun mysqlDataListUpload(Lati: String, Longi: String) {
        thread {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver")
            // 建立连接
            val cn = DriverManager.getConnection(
                "jdbc:mysql://112.74.87.145:3306/runart",
                "runartadmin", "runartpassword"
            )

            val psCreateUser: PreparedStatement = cn.prepareStatement(
                "INSERT INTO `Grp_$grpId` (`userId`, `lati`, `longi`) VALUES (?, ?, ?)"
            )
            if (resourceId != "localhost") {
                psCreateUser.setInt(1, resourceId.toInt())
                psCreateUser.setString(2, Lati)
                psCreateUser.setString(3, Longi)
                psCreateUser.executeUpdate()
            }
            psCreateUser.close()
            if (!cn.isClosed){
                cn.close()
            }
        }
    }

    private fun mysqlDataListDownload() {
        thread {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver")
            // 建立连接
            val cn = DriverManager.getConnection(
                "jdbc:mysql://112.74.87.145:3306/runart",
                "runartadmin", "runartpassword"
            )

            val cs = cn.createStatement()
            val resultSet: ResultSet = cs.executeQuery(
                "SELECT * FROM `Grp_$grpId` WHERE id > $idTag AND userId != $resourceId"
            )
            if (resultSet.first()) {
                do {
                    val tmpBundle = Bundle()
                    tmpBundle.putInt("userId", resultSet.getInt("userId"))
                    tmpBundle.putString("lati", resultSet.getNString("lati"))
                    tmpBundle.putString("longi", resultSet.getNString("longi"))
                    idTag = resultSet.getInt("id")
                    val msg = Message()
                    msg.what = 666
                    msg.data = tmpBundle
                    handler.sendMessage(msg)
                } while (resultSet.next())
            }
            if (!cn.isClosed) {
                cn.close()
            }
        }
    }
    @SuppressLint("Range")
    override fun onReceiveLocation(location: BDLocation) {
        if (mMapView == null || location == null) {
            return
        }
        if (!isNetworkConnected(context)){
            return
        }
        //导航至当前位置
        var ll = LatLng(location.latitude, location.longitude)
        var update: MapStatusUpdate = MapStatusUpdateFactory.newLatLng(ll)
        mBaiduMap.animateMapStatus(update)
        //var update:MapStatusUpdate = MapStatusUpdateFactory.zoomTo()
        var locationBuilder = MyLocationData.Builder()
        locationBuilder.longitude(location.longitude)
        locationBuilder.latitude(location.latitude)
        locationBuilder.direction(location.direction)
        var locationData: MyLocationData = locationBuilder.build()
        mBaiduMap.setMyLocationData(locationData)
        
        //调用导航
        if (isFirstLocate) {
            navigateToNow(location)
            lastLocation = LatLng(location.latitude, location.longitude)
        }
        if (resourceId != "localhost" && grpId != 0){
            mysqlDataListDownload()
        }
        
        if (isOnDraw) {
            Log.d("draw", "paole")
            if (isFirstLoc) {
                //首次定位
                var ll: LatLng? = null
                
                //选一个精度相对较高的起始点
                //ll = getMostAccuracyLocation(location)
                ll = LatLng(location.latitude, location.longitude)
                if (ll == null) {
                    return
                }
                
                isFirstLoc = false
                MapActivity.Datalist[0].points.add(ll) //加入集合
                //TODO:上传第一个点 经纬度：ll.latitude ll.longitude
                if (resourceId != "localhost" && grpId != 0){
                    mysqlDataListUpload(ll.latitude.toString(), ll.longitude.toString())
                }
                last = ll
                
                //构建Marker图标
                val startBD = BitmapDescriptorFactory
                    .fromResource(R.drawable.pic_start)
                //标记起点图层位置
                val oStart = MarkerOptions() // 地图标记覆盖物参数配置类
                oStart.position(MapActivity.Datalist[0].points[0]) // 覆盖物位置点，第一个点为起点
                oStart.icon(startBD) // 设置覆盖物图片
                mBaiduMap.addOverlay(oStart) // 在地图上添加此图层
                return
            }
            
            //从第二个点开始
            val ll = LatLng(location.latitude, location.longitude)
            //TODO:上传点 经纬度：ll.latitude ll.longitude
            if (resourceId != "localhost" && grpId != 0){
                mysqlDataListUpload(ll.latitude.toString(),ll.longitude.toString())
            }
    
            MapActivity.Datalist[0].points.add(ll)
            last = ll
            mMapView.map.clear()
            
            //构建Marker图标
            val startBD = BitmapDescriptorFactory
                .fromResource(R.drawable.pic_start)
            //起始点图层也会被清除，重新绘画
            val oStart = MarkerOptions()
            oStart.position(MapActivity.Datalist[0].points[0])
            oStart.icon(startBD)
            mBaiduMap.addOverlay(oStart)
            
            //将points集合中的点绘制轨迹线条图层，显示在地图上
            for (item in MapActivity.Datalist){
                // TODO: id -> color
                val dbHelper = AppDatabaseHelper(MyApplication.context, "BookStore.db", 1)
                val db = dbHelper.writableDatabase
                val cursorGroup = db.rawQuery(
                    "SELECT * FROM Colors WHERE id = ? % 40", arrayOf(item.userId.toString())
                )
                val lineColor: Int =
                    if (cursorGroup.moveToFirst()){
                        -Color.parseColor(cursorGroup.getString(cursorGroup.getColumnIndex("color_code")))
                    } else {
                        -0x55010000
                    }
                cursorGroup.close()
                val ooPolyline: OverlayOptions =
                    PolylineOptions().width(13).color(lineColor).points(item.points)
                var mPolyline = mBaiduMap.addOverlay(ooPolyline) as Polyline
            }
            //TODO:绘制曲线
            
            /*
            //这里是完整的绘制过程 points是LatLng 的 arrayList
            val ooPolyline: OverlayOptions =
                PolylineOptions().width(13).color(-0x55010000).points(points)
            var mPolyline = mBaiduMap.addOverlay(ooPolyline) as Polyline
            */
            
        } else {
            //停止绘制

            if (MapActivity.Datalist[0].points.size <= 0) {
                return
            }
            //构建Marker图标
            val finishBD = BitmapDescriptorFactory
                .fromResource(R.drawable.pic_end)
            //运动结束标记终点图标
            val oFinish = MarkerOptions()
            oFinish.position(MapActivity.Datalist[0].points[MapActivity.Datalist[0].points.size - 1])
            oFinish.icon(finishBD)
            mBaiduMap.addOverlay(oFinish)
        }
        
        MapActivity.zoom = getZoom(mMapView.mapLevel)
    }
}

//导航至当前位置的函数
fun navigateToNow(location: BDLocation) {
    var ll = LatLng(location.latitude, location.longitude)
    var update: MapStatusUpdate = MapStatusUpdateFactory.newLatLng(ll)
    mBaiduMap.animateMapStatus(update)
    // 缩放地图大小
    update = MapStatusUpdateFactory.zoomTo(MapActivity.zoom)
    mBaiduMap.animateMapStatus(update)
    isFirstLocate = false
}

//初始化
fun initOption(option: LocationClientOption) {
    option.locationMode = LocationClientOption.LocationMode.Hight_Accuracy
    option.setCoorType("bd09ll")
    option.setScanSpan(1000);//发起定位请求的间隔
    option.isOpenGps = true//打开GPS
    option.isLocationNotify = true
    option.setEnableSimulateGps(false)
    option.setIsNeedAddress(true)
    option.setNeedDeviceDirect(true)
}

//绘制轨迹
@SuppressLint("Range")
fun drawTrace(location: BDLocation) {
    Log.d("draw", "paole")
    var points: ArrayList<LatLng> = ArrayList()
    
    val location: LatLng = mBaiduMap.mapStatus.target
    val averageLocation = LatLng(
        ((lastLocation.latitude + location.latitude) / 2),
        ((lastLocation.longitude + location.longitude) / 2)
    )
    val la = location.latitude
    Log.d("location", "$la")
    Log.d("location last", lastLocation.toString())
    Log.d("location ave", averageLocation.toString())
    Log.d("location now", location.toString())
    points.add(lastLocation)
    points.add(averageLocation)
    points.add(location)
    
    /*if (location != lastLocation){

        //设置折线的属性
        val mArcOptions: OverlayOptions = ArcOptions()
            .color(Color.RED)
            .width(10)
            .points(lastLocation,averageLocation,location)
        //在地图上绘制折线
        //mPloyline 折线对象
        val mPolyline = mBaiduMap.addOverlay(mArcOptions)

        lastLocation = location
    }*/
    
    //设置折线的属性
    //构造PolygonOptions
    for (item in MapActivity.Datalist){
        val lineColor: Int = 0
        val mPolygonOptions = PolygonOptions()
            .points(item.points)
            .fillColor(lineColor) //填充颜色
            .stroke(Stroke(5, lineColor)) //边框宽度和颜色
    
        //在地图上显示多边形
        mBaiduMap.addOverlay(mPolygonOptions)
    }
}

//找不到获取缩放等级的方法，故重写
fun getZoom(level: Int): Float {
    when (level) {
        1000000 -> {
            return 4f
        }
        500000 -> {
            return 5f
        }
        200000 -> {
            return 6f
        }
        100000 -> {
            return 7f
        }
        50000 -> {
            return 8f
        }
        25000 -> {
            return 9f
        }
        20000 -> {
            return 10f
        }
        10000 -> {
            return 11f
        }
        5000 -> {
            return 12f
        }
        2000 -> {
            return 13f
        }
        1000 -> {
            return 14f
        }
        500 -> {
            return 15f
        }
        200 -> {
            return 16f
        }
        100 -> {
            return 17f
        }
        50 -> {
            return 18f
        }
        20 -> {
            return 19f
        }
        10 -> {
            return 20f
        }
        5 -> {
            return 21f
        }
    }
    return 19f
}

//选精度相对较高的起始点
//private fun getMostAccuracyLocation(location: BDLocation): LatLng? {
//    if (location.radius > 25) { //gps位置精度大于25米的点直接弃用
//        return null
//    }
//    val ll = LatLng(location.latitude, location.longitude)
//
//    Log.d("dis", DistanceUtil.getDistance(last, ll).toString())
//    if (DistanceUtil.getDistance(last, ll) > 5) {
//        last = ll
//        points.clear() //有两点位置大于5，重新来过
//        return null
//    }
//    points.add(ll)
//    last = ll
//    //有5个连续的点之间的距离小于5，认为gps已稳定，以最新的点为起始点
//    if (points.size >= 5) {
//        points.clear()
//        return ll
//    }
//    return null
//}

