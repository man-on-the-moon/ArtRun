package com.example.artrun

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artrun.databinding.ActivityCreateNewGroupBinding
import com.example.artrun.databinding.ActivityJoinGroupBinding
import com.example.artrun.databinding.ActivityMapAcitivityBinding
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread

class JoinGroupActivity : AppCompatActivity() {
    private lateinit var binding: ActivityJoinGroupBinding
    val groupList = ArrayList<Group>()
    
    val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what){
                666 -> {
                    val resourceId = msg.data.getInt("id")
                    val resourceName = msg.data.getString("grpName") ?: ""
                    val resourceNum = msg.data.getInt("grpNum")
                    groupList.add(Group(resourceId, resourceName, resourceNum))
                }
                707 -> {
                    binding.recyclerGroupManager.layoutManager = LinearLayoutManager(this@JoinGroupActivity)
                    binding.recyclerGroupManager.adapter = GroupAdapter(this@JoinGroupActivity, groupList)
                }
            }
        }
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJoinGroupBinding.inflate(layoutInflater)
        setContentView(binding.root)
    
        groupList.clear()
        mysqlGroupList()
        
        binding.btnExitJoinActivity.setOnClickListener {
            finish()
        }
    }
    
    private fun mysqlGroupList() {
        thread {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver")
            // 建立连接
            val cn = DriverManager.getConnection(
                "jdbc:mysql://112.74.87.145:3306/runart",
                "runartadmin", "runartpassword"
            )
    
            val cs = cn.createStatement()
            val resultSet: ResultSet = cs.executeQuery(
                "SELECT * FROM `Group`"
            )
            if (resultSet.first()) {
                do {
                    val tmpBundle = Bundle()
                    tmpBundle.putInt("id", resultSet.getInt("id"))
                    tmpBundle.putString("grpName", resultSet.getString("grpName"))
                    tmpBundle.putInt("grpNum", resultSet.getInt("grpNum"))
    
                    val msg = Message()
                    msg.what = 666
                    msg.data = tmpBundle
                    handler.sendMessage(msg)
                } while (resultSet.next())
                val msgFinish = Message()
                msgFinish.what = 707
                handler.sendMessage(msgFinish)
            }
            if (!cn.isClosed) {
                cn.close()
            }
        }
    }
}