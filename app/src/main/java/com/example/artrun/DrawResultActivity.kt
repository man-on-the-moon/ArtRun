package com.example.artrun

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.baidu.location.BDAbstractLocationListener
import com.baidu.location.BDLocation
import com.baidu.location.LocationClient
import com.baidu.location.LocationClientOption
import com.baidu.mapapi.map.*
import com.baidu.mapapi.model.LatLng
import com.example.artrun.databinding.ActivityDrawResultBinding


@SuppressLint("StaticFieldLeak")
private lateinit var mMapView: MapView
private lateinit var mBaiduMap: BaiduMap
private lateinit var binding: ActivityDrawResultBinding
@SuppressLint("StaticFieldLeak")
private lateinit var mLocationClient: LocationClient
private lateinit var option: LocationClientOption
private lateinit var myLocationListener: MyResultLocationListener
class DrawResultActivity : AppCompatActivity() {
    @SuppressLint("Range")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDrawResultBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mMapView = binding.bmapViewResult
        //开启定位
        mBaiduMap = mMapView.map
        mBaiduMap.mapType = BaiduMap.MAP_TYPE_NONE
        mBaiduMap.isMyLocationEnabled = true
        val settings = mBaiduMap.uiSettings
        settings.setAllGesturesEnabled(false)
        mBaiduMap.setMyLocationConfiguration(
            MyLocationConfiguration(
                MyLocationConfiguration.LocationMode.FOLLOWING, true, null
            )
        )
        //定位初始化
        mLocationClient = LocationClient(this)
        //通过LocationClientOption设置LocationClient相关参数
        option = LocationClientOption()
        //初始化
        initOption(option)
        mLocationClient.setLocOption(option)
        //注册LocationListener监听器
        myLocationListener = MyResultLocationListener()
        mLocationClient.registerLocationListener(myLocationListener)
        //开启地图定位图层
        mLocationClient.start()

        //将points集合中的点绘制轨迹线条图层，显示在地图上
        for (item in MapActivity.Datalist){
            // TODO:
            val dbHelper = AppDatabaseHelper(MyApplication.context, "BookStore.db", 1)
            val db = dbHelper.writableDatabase
            val cursorGroup = db.rawQuery(
                "SELECT * FROM Colors WHERE id = ? % 40", arrayOf(item.userId.toString())
            )
            val lineColor: Int =
                if (cursorGroup.moveToFirst()){
                    -Color.parseColor(cursorGroup.getString(cursorGroup.getColumnIndex("color_code")))
                } else {
                    -0x55010000
                }
            cursorGroup.close()
            val ooPolyline: OverlayOptions =
                PolylineOptions().width(13).color(lineColor).points(item.points)
            var mPolyline = mBaiduMap.addOverlay(ooPolyline) as Polyline
        }

        binding.btnExitResult.setOnClickListener {
            mLocationClient.stop()
            mBaiduMap.isMyLocationEnabled = false
            mMapView.removeAllViews()
            mMapView.onDestroy()
            val intentMainActivity = Intent(this,MainActivity::class.java)
            startActivity(intentMainActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
    }

    //释放资源
    override fun onDestroy() {
        super.onDestroy()
        mLocationClient.stop()
        mBaiduMap.isMyLocationEnabled = false
        mMapView.removeAllViews()
        mMapView.onDestroy()
    }

    //初始化
    private fun initOption(option: LocationClientOption) {
        option.locationMode = LocationClientOption.LocationMode.Hight_Accuracy
        option.setCoorType("bd09ll")
        option.setScanSpan(1000);//发起定位请求的间隔
        option.isOpenGps = true//打开GPS
        option.isLocationNotify = true
        option.setEnableSimulateGps(false)
        option.setIsNeedAddress(true)
        option.setNeedDeviceDirect(false)
    }
}

class MyResultLocationListener : BDAbstractLocationListener() {
    override fun onReceiveLocation(location: BDLocation) {
        if (mMapView==null||location==null){
            return
        }
        //导航至当前位置
        var ll = LatLng(location.latitude, location.longitude)
        var updateloc: MapStatusUpdate = MapStatusUpdateFactory.newLatLng(ll)
        mBaiduMap.animateMapStatus(updateloc)
        // 缩放地图大小
        var update = MapStatusUpdateFactory.zoomTo(MapActivity.zoom)
        mBaiduMap.animateMapStatus(update)

    }
}