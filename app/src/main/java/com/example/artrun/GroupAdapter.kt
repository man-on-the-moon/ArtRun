package com.example.artrun

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import kotlin.concurrent.thread

class GroupAdapter(val context: Context, val groupList: List<Group>) : RecyclerView.Adapter<GroupAdapter.ViewHolder>() {
    private var resourceId: String = "localhost"
    
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val grpId: TextView = view.findViewById(R.id.GroupId)
        val grpName: TextView = view.findViewById(R.id.GroupName)
        val grpNum: TextView = view.findViewById(R.id.GroupNum)
        val box: LinearLayout = view.findViewById(R.id.item_box)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.group_item, parent, false)
        val holder = ViewHolder(view)
        return holder
    }

    @SuppressLint("Range", "SetTextI18n")
    override fun onBindViewHolder(holder: GroupAdapter.ViewHolder, position: Int) {
        val group = groupList[position]
        holder.grpId.text = group.id.toString()
        holder.grpName.text = group.name
        holder.grpNum.text = group.number.toString() + "人"
        holder.box.setOnClickListener {
            val dbHelper = AppDatabaseHelper(context, "BookStore.db", 1)
            val db = dbHelper.writableDatabase
            getLoginTag(db)
            db.execSQL(
                "update User set grpId = ? WHERE id = ?",
                arrayOf(group.id, resourceId)
            )
            mysqlJoinGroup(group.id, resourceId.toInt())
            Toast.makeText(context, "已加入 ${group.name}", Toast.LENGTH_SHORT).show()
            (context as Activity).finish()
        }
    }

    override fun getItemCount() = groupList.size
    
    @SuppressLint("Range")
    private fun getLoginTag(db: SQLiteDatabase) {
        val cursorLoginTag = db.rawQuery("select * from User where login_tag = 1", null)
        resourceId = if (cursorLoginTag.moveToFirst()) {
            cursorLoginTag.getString(cursorLoginTag.getColumnIndex("id"))
        } else {
            "localhost"
        }
        cursorLoginTag.close()
    }
    
    private fun mysqlJoinGroup(resourceGrpId: Int, resourceId: Int) {
        thread {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver")
            // 建立连接
            val cn = DriverManager.getConnection(
                "jdbc:mysql://112.74.87.145:3306/runart",
                "runartadmin", "runartpassword"
            )
    
            var psUpdateCustom: PreparedStatement = cn.prepareStatement(
                "UPDATE `User` SET `grpId`=? WHERE `id`=?"
            )
            psUpdateCustom.setInt(1, resourceGrpId)
            psUpdateCustom.setInt(2, resourceId)
            psUpdateCustom.executeUpdate()
            psUpdateCustom = cn.prepareStatement(
                "UPDATE `Group` SET `grpNum` = `grpNum`+1 WHERE `id`=?"
            )
            psUpdateCustom.setInt(1, resourceGrpId)
            psUpdateCustom.executeUpdate()
            psUpdateCustom.close()
        }
    }
}