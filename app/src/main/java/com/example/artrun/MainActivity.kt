package com.example.artrun

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.baidu.mapapi.CoordType
import com.baidu.mapapi.SDKInitializer
import com.example.artrun.databinding.ActivityMainBinding
import java.sql.DriverManager
import java.sql.PreparedStatement
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var resourceId: String = "localhost"
    private var resourceGrpId: Int = 0
    
    @SuppressLint("Range")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        
        val dbHelper = AppDatabaseHelper(this, "BookStore.db", 1)
        val db = dbHelper.writableDatabase
        getLoginTag(db)
        try {

            val hex = "-0x552254cf"
            val convertedValue = Integer.decode(hex)
            Log.d("sehao","$convertedValue")

        }catch (e:Exception){
            Log.d("sehao","ji")
        }
    
        //百度地图 在使用SDK各组件之前初始化context信息，传入ApplicationContext
        SDKInitializer.initialize(applicationContext)
        SDKInitializer.setCoordType(CoordType.BD09LL)
        setContentView(binding.root)
        //申请定位权限
        binding.btnMapActivity.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            } else {
                val intentMapActivity = Intent(this, MapActivity::class.java)
                startActivity(intentMapActivity)
            }
        }
        
        binding.btnLoginActivity.setOnClickListener {
            if (resourceId == "localhost"){
                val intentLoginActivity = Intent(this, LoginActivity::class.java)
                startActivity(intentLoginActivity)
            }
            else {
                // 获取账号，名称，头像地址
                val cursor = db.rawQuery("select * from User where login_tag = 1", null)
                if (cursor.moveToFirst()) {
                    // 存在登录账号
                    db.execSQL(
                        "update User set login_tag = ? where account = ?",
                        arrayOf(0, cursor.getString(cursor.getColumnIndex("account")))
                    )
                    Toast.makeText(this, "已退出登录", Toast.LENGTH_SHORT).show()
                    getLoginTag(db)
                    setResult(Activity.RESULT_OK)
                }
                cursor.close()
                binding.btnLoginActivity.text = "点击登录"
                binding.btnLoginActivity.setTextColor(Color.BLACK)
            }
        }
        binding.btnMoreMain.setOnClickListener {
            showRepeatModeSelect(binding.btnMoreMain)
        }
    }
    
    override fun onResume() {
        super.onResume()
        // init 数据库
        val dbHelper = AppDatabaseHelper(this, "BookStore.db", 1)
        val db = dbHelper.writableDatabase
        getLoginTag(db)
        if (resourceId != "localhost"){
            binding.btnMoreMain.visibility = View.VISIBLE
            binding.btnLoginActivity.text = "点击退出登录"
            binding.btnLoginActivity.setTextColor(Color.GRAY)
        }
        else {
            binding.btnMoreMain.visibility = View.INVISIBLE
            binding.btnLoginActivity.text = "点击登录"
            binding.btnLoginActivity.setTextColor(Color.BLACK)
        }
    }
    
    @SuppressLint("Range")
    private fun getLoginTag(db: SQLiteDatabase) {
        val cursorLoginTag = db.rawQuery("select * from User where login_tag = 1", null)
        if (cursorLoginTag.moveToFirst()) {
            resourceId = cursorLoginTag.getString(cursorLoginTag.getColumnIndex("id"))
            resourceGrpId = cursorLoginTag.getInt(cursorLoginTag.getColumnIndex("grpId"))
        } else {
            resourceId = "localhost"
        }
        cursorLoginTag.close()
    }
    
    @SuppressLint("Range")
    private fun showRepeatModeSelect(view: View) {
        val popup = PopupMenu(this, view)
        popup.inflate(R.menu.grp_select_menu)
    
        popup.menu.add(Menu.NONE, 1, 1, "创建新小队")
        if (resourceGrpId != 0){
            popup.menu.add(Menu.NONE, 3, 1, "退出小队")
        }
        else {
            popup.menu.add(Menu.NONE, 2, 1, "加入小队")
        }
        
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem ->
            when(item.itemId){
                1 -> {
                    val intent = Intent(this, CreateNewGroupActivity::class.java)
                    startActivity(intent)
                }
                2 -> {
                    val intent = Intent(this, JoinGroupActivity::class.java)
                    startActivity(intent)
                }
                3 -> {
                    val dbHelper = AppDatabaseHelper(this, "BookStore.db", 1)
                    val db = dbHelper.writableDatabase
                    db.execSQL(
                        "update User set grpId = ? WHERE id = ?",
                        arrayOf(0, resourceId)
                    )
                    mysqlExitGroup(resourceGrpId, resourceId.toInt())
                    getLoginTag(db)
                    Toast.makeText(this, "退出成功", Toast.LENGTH_SHORT).show()
                }
            }
            true
        })
        popup.show()
    }
    
    private fun mysqlExitGroup(resourceGrpId: Int, resourceId: Int) {
        thread {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver")
            // 建立连接
            val cn = DriverManager.getConnection(
                "jdbc:mysql://112.74.87.145:3306/runart",
                "runartadmin", "runartpassword"
            )
            
            var psUpdateCustom: PreparedStatement = cn.prepareStatement(
                "UPDATE `User` SET `grpId`=0 WHERE `id`=?"
            )
            psUpdateCustom.setInt(1, resourceId)
            psUpdateCustom.executeUpdate()
            psUpdateCustom = cn.prepareStatement(
                "UPDATE `Group` SET `grpNum` = `grpNum`-1 WHERE `id`=?"
            )
            psUpdateCustom.setInt(1, resourceGrpId)
            psUpdateCustom.executeUpdate()
            psUpdateCustom.close()
            if (!cn.isClosed) {
                cn.close()
            }
        }
    }
    
    //权限请求
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    val intentMapActivity = Intent(this, MapActivity::class.java)
                    startActivity(intentMapActivity)
                } else {
                    Toast.makeText(
                        this, "You denied the permission",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}