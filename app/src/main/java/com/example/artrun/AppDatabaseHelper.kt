package com.example.artrun

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

class AppDatabaseHelper(val context: Context, name: String, version: Int) :
    SQLiteOpenHelper(context, name, null, version) {
    private val createUser = "CREATE TABLE User (" +
            "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
            "account TEXT NOT NULL UNIQUE," +
            "grpId INTEGER NOT NULL DEFAULT 0," +
            "create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP," +
            "login_tag INTEGER NOT NULL DEFAULT 0)"
    
    private val createColor = "CREATE TABLE Colors (" +
            "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
            "color_code TEXT)"
    
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(createUser)
        db.execSQL(createColor)
        insertColorGroup(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}
    
    // 插入颜色表
    private fun insertColorGroup(db: SQLiteDatabase){
        // more: 自定义分组
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#552254CF")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#555ea6fa")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55fc407a")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#552456db")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f4d352")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#556efee5")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f69850")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#556a40fc")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#554b7be7")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55fc6d43")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#551d409e")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#555ed1ec")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#559dfc58")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55ab3efc")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5582b4fb")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#558df1d5")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f0829b")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#559670fb")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55ea56f2")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55854ffb")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55566fff")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#555ec6fd")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55b255fc")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#555c9afd")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5590a2f6")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55dfa4f6")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55d4cdf6")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#554b7fd2")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f47ce8")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#558d3fc6")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#559f3891")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55672d94")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5591d1f4")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5559afde")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55b08af9")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55e14fb4")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55335d8f")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55b348d6")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#554688ab")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55842f6a")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55b3bcfd")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#553e5dad")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55edc4e6")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f399cd")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5598d5f4")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55bef4d3")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f6ee9b")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55969dfb")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f3baa9")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#554cbcfb")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#553685b0")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55a0fcea")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55bfecff")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5561cefb")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55215d7a")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5588b6ce")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55fc92b6")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#557396a9")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55f8a993")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55d69285")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55c7c57e")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55cd7dba")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55b8baf3")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#557e7fd1")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#557ec486")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#559efbaa")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#5545a0cf")
        )
        
        db.execSQL(
            "insert into Colors (color_code) values(?)", arrayOf("#55bb7d72")
        )
    }
}