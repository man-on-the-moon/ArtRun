Android Studio 开发
#### 使用说明
1.  功能开发前要pull master / 个人 分支最新代码
2.  修改后代码要先push到个人分支中，再pull master分支，处理可能出现的冲突代码，最后push到master分支
3.  开发未完成的代码不能push到master分支（至少不能有报错）
4.  commit尽量说明修改 / 添加的代码含义
5.  对自定义class / function须有简要注释
6.  id定义规范：
    -  主Id: 控件类型 + 功能  e.g. btnBack
    -  从属Id: 控件所属Activity  e.g. btnBack_account_manage
7.  id定义的必要性: 需要进行到事件操作的控件才定义Id, 静态控件尽量避免Id的定义, 默认的Id必须禁止出现
#### 其他说明
-   开发过程尽量保持代理稳定
-   非代码修改的资料直接web端上传即可
-   想到了再说
